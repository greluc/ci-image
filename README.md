# [CI Image](https://gitlab.com/greluc/ci-image/)
[![pipeline status](https://gitlab.com/greluc/ci-image/badges/main/pipeline.svg?style=flat-square)](https://gitlab.com/greluc/ci-image/pipelines)
[![license](https://img.shields.io/badge/License-MIT-blue.svg?style=flat-square)](LICENSE.md)
[![code of conduct](https://img.shields.io/badge/Contributor%20Covenant-v2.1%20adopted-ff69b4.svg?style=flat-square)](CODE_OF_CONDUCT.md)

## Description
CI Images used by the other projects of @greluc.

## Current release version
See [Container Registry](https://gitlab.com/greluc/ci-image/container_registry).

## [License](LICENSE.md)
CI Image is licensed under the MIT License. You can find the license text under [LICENSE](LICENSE.md).

## [Contribution](CONTRIBUTING.md)
+ Please read [CONTRIBUTING](CONTRIBUTING.md) when you wish to contribute to this project.
+ Please note that this project is released with a Contributor [CODE_OF_CONDUCT](CODE_OF_CONDUCT.md). By participating in this project you agree to abide by its terms.
