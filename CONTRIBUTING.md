# Contributing to *CI Image*

First off, thanks for taking the time to contribute!

The following is a set of guidelines for contributing to CI Image - CI Image, which is hosted with GitLab. These are mostly guidelines, not rules. Use your best judgment, and feel free to propose changes to this document in a merge request.

## Table Of Contents

+ [Code of Conduct](#code-of-conduct)
+ [I don't want to read this whole thing, I just have a question!!!](#i-dont-want-to-read-this-whole-thing-i-just-have-a-question)
+ [How Can I Contribute?](#how-can-i-contribute)
  + [Reporting Bugs](#reporting-bugs)
  + [Suggesting Enhancements](#suggesting-enhancements)
  + [Your First Code Contribution](#your-first-code-contribution)
  + [Merge Requests](#merge-requests)
+ [Style Guides](#style-guides)

# Code of Conduct

This project and everyone that participates in it is governed by the [Code of Conduct](CODE_OF_CONDUCT.md). By participating, you are expected to uphold this code. Please report unacceptable behavior to [lucas.greuloch@gmail.com](mailto:lucas.greuloch@gmail.com).

# I don't want to read this whole thing I just have a question!!!

First, you can read the documentation or search the wiki for the information you need.

If you need help by the developers, create an issue with your question and add the label **support** to it. **Perform a cursory search** to see if your question has already been asked. If it has, add a comment to the existing issue instead of opening a new one.

> **Note:** If you find a **Closed** issue that seems like it is the same topic that you're looking for, open a new issue and include a link to the original issue in the body of your new one.

# How Can I Contribute?

## Reporting Bugs

This section guides you through submitting a bug report for CI Image - CI Image. Following these guidelines helps maintainers and the community to understand your report, reproduce the behavior, and find related reports.

Before creating bug reports, please check [this list](#before-submitting-a-bug-report) as you might find out that you don't need to create one. When you are creating a bug report, please [include as many details as possible](#how-do-i-submit-a-good-bug-report). Fill out [the required template](.gitlab/issue_templates/Bug.md), the information it asks for helps us to resolve issues faster.


### Before Submitting A Bug Report

* **Perform a cursory search** to see if the problem has already been reported. If it has **and the issue is still open**, add a comment to the existing issue instead of opening a new one.

> **Note:** If you find a **Closed** issue that seems like it is the same thing that you're experiencing, open a new issue and include a link to the original issue in the body of your new one.

### How Do I Submit A (Good) Bug Report?

Bugs are tracked as Issues. Provide the following information by filling in [the template](.gitlab/issue_templates/Bug.md).

> **Note:** Instead of creating an issue through the WebUI of GitLab you can send your bug report via mail to our service desk at [incoming+project-inventory-database-ci-images-18202121-issue-@incoming.gitlab.com](mailto:incoming+project-inventory-database-ci-images-18202121-issue-@incoming.gitlab.com).

Explain the problem and include additional details to help maintainers reproduce the problem:

* **Use a clear and descriptive title** for the issue to identify the problem.
* **Describe the exact steps which reproduce the problem** in as many details as possible. For example, start by explaining how you started the software. When listing steps, **don't just say what you did, but explain how you did it**.
* **Provide specific examples to demonstrate the steps.** Include links to files,other projects or copy/pasteable snippets, which you use in those examples. If you're providing snippets in the issue, use [Markdown code blocks](https://gitlab.com/help/user/markdown#code-and-syntax-highlighting).
* **Describe the behavior you observed after following the steps** and point out what exactly is the problem with that behavior.
* **Explain which behavior you expected to see instead and why.**
* **Include screenshots and animated GIFs** which show you following the described steps and clearly demonstrate the problem.
* **If you're reporting that CI Image - CI Image crashed**, include a crash report with a stack trace from the operating system. Include the crash report in the issue in a [code block](https://gitlab.com/help/user/markdown#code-and-syntax-highlighting), a file attachment, or put it in a [snippet](https://gitlab.com/dashboard/snippets) and provide the link to that snippet.
* **If the problem wasn't triggered by a specific action**, describe what you were doing before the problem happened and share more information using the guidelines below.

Provide more context by answering these questions:

* **Did the problem start happening recently** (e.g. after updating to a new version of CI Image - CI Image) or was this always a problem?
* If the problem started happening recently, **can you reproduce the problem in an older version of CI Image - CI Image?** What's the most recent version in which the problem doesn't happen? You can download older versions of CI Image - CI Image from the releases' directory.
* **Can you reliably reproduce the issue?** If not, provide details about how often the problem happens and under which conditions it normally happens.
* If the problem is related to working with files (e.g. opening and editing files), **does the problem happen for all files and projects or only some?** Is there anything else special about the files you are using?

Include details about your configuration and environment:

* **Which version of CI Image - CI Image are you using?**
* **What's the name and version of the OS you're using**?
* **What's your Java version and type** (JRE or JDK)**?**
* **What's your Java distribution** (e.g. Oracle, OpenJDK, AdoptOpenJDK, Amazon Coretto, IBM, Azul Zulu, ...)**?**
* **Are you running CI Image - CI Image in a virtual machine?** If so, which VM software are you using and which operating systems and versions are used for the host and the guest?

## Suggesting Enhancements

This section guides you through submitting an enhancement suggestion for CI Image - CI Image, including completely new features and minor improvements to existing functionality. Following these guidelines helps maintainers and the community understand your suggestion and find related suggestions.

Before creating enhancement suggestions, please check [this list](#before-submitting-an-enhancement-suggestion) as you might find out that you don't need to create one. When you are creating an enhancement suggestion, please [include as many details as possible](#how-do-i-submit-a-good-enhancement-suggestion). Fill in [the template](.gitlab/issue_templates/Feature.md), including the steps that you imagine you would take if the feature you're requesting existed.

### Before Submitting An Enhancement Suggestion

* **Perform a cursory search** to see if the enhancement has already been suggested. If it has, add a comment to the existing issue instead of opening a new one.

> **Note:** If you find a **Closed** issue that seems like it is the same thing that you're suggesting, and you have new arguments to implement ist, open a new issue and include a link to the original issue in the body of your new one.

### How Do I Submit A (Good) Enhancement Suggestion?

Enhancement suggestions are tracked as Issues. Create an issue and provide the following information:

* **Use a clear and descriptive title** for the issue to identify the suggestion.
* **Provide a step-by-step description of the suggested enhancement** in as many details as possible.
* **Provide specific examples to demonstrate the steps**. Include copy / pasteable snippets which you use in those examples, as [Markdown code blocks](https://gitlab.com/help/user/markdown#code-and-syntax-highlighting).
* **Describe the current behavior** and **explain which behavior you expected to see instead** and why.
* **Include screenshots and animated GIFs** which help you demonstrate the steps or point out the part which the suggestion is related to.
* **Explain why this enhancement would be useful** to most CI Image - CI Image users.
* **Specify which version of CI Image - CI Image you're using.**
* **Specify the name and version of the OS you're using.**

## Your First Code Contribution

Unsure where to begin contributing to CI Image - CI Image? You can start by looking through these `beginner` and `help-wanted` issues:

* **Beginner issues** - issues which should only require a few lines of code, and a test or two.
* **Help wanted issues** - issues which should be a bit more involved than `beginner` issues.

## Merge Requests

* Fill in [the required template](.gitlab/merge_request_templates/Merge_Request.md)
* Do not include issue numbers in the title
* Include screenshots and animated GIFs in your merge request whenever possible.
* Document new code based on the [Style Guides](#style-guides)
* End all files with a newline
* Avoid platform-dependent code

# Style Guides

## Git Commit Messages Style Guide

* Use the present tense ("Add feature" not "Added feature")
* Use the imperative mood ("Move cursor to..." not "Moves cursor to...")
* Limit the first line to 72 characters or fewer
* Reference issues and merge requests liberally after the first line (`relates to #XYZ` or `relates to !XYZ` or `relates to NULL`)
* Add `spend XYZ` to track the time you spend for this commit (and remember to add the time to the issue!)
* Add `[skip ci]` to the commit message to skip the ci pipeline if it doesn't need to run
* Consider starting the commit message with an applicable emoji:

| Emoji | Markdown Code | Description |
| ------ | ------ | ------ |
| :construction: | `:construction:` | when improving the format/structure of the code or adding new features |
| :racehorse: | `:racehorse:` | when improving performance |
| :non-potable_water: | `:non-potable_water:` | when plugging memory leaks |
| :art: | `:art:` | when adding or updating assets |
| :memo: | `:memo:` | when writing docs |
| :bug: | `:bug:` | when fixing a bug |
| :wrench: | `:wrench:` | when fixing bugs that are not os-specific |
| :penguin: | `:penguin:` | when fixing something on Linux |
| :apple: | `:apple:` | when fixing something on macOS |
| :checkered_flag: | `:checkered_flag:` | when fixing something on Windows |
| :fire: | `:fire:` | when removing code or files |
| :green_heart: | `:green_heart:` | when enhancing/fixing the CI build |
| :white_check_mark: | `:white_check_mark:` | when adding/fixing tests |
| :lock: | `:lock:` | when dealing with security |
| :arrow_up: | `:arrow_up:` | when upgrading dependencies |
| :arrow_down: | `:arrow_down:` | when downgrading dependencies |
